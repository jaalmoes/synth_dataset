# Synthetic LAW dataset
Base law dataset in `real.csv`.
The rest of the csv(s) files are synthetic generated datasets.

## Generation method used  
 - CTGAN 
 - Synthpop

 ## Cross-validation
For each method cross validation (5-folding) is used and the folding numbers from 0 to 4 are indicated in the csv(s) file names.
The indices used for each fold for training and validation are in the `ids/train/k.txt` and `ids/tests/k.txt` files.
Those indices corresonds to the orginial data used to generate the synthetic datasets.
